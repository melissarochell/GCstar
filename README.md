# GCstar
Management of collections (films, music, games, books, etc...)

(version française ci-dessous)

## Software

The application is written in Perl with Gtk modules : it runs on Linux, Windows and MacOS. 


## Project

The [GCstar](http://www.gcstar.org/) was initiated by Tian and the source code was available under GPL licence on the [SVN repository](http://gna.org/projects/gcstar) (closed in May 2017). Up to recently, there was also a [discussion forum](http://forums.gcstar.org/) but it's currently unavailable for security reasons. 

GCstar uses a plugin system to gather information on collection items by scraping various web sites. The development of these plugins is currently very distributed : some appear in forums ( https://forum.ubuntu-fr.org/viewtopic.php?id=1919911 , http://forums.gcstar.org/ ), others in open source repositories. Plugins can be updated without reinstalling GCstar.

This repository was created directly from the SVN repo using **git-svn**, trying to preserve tags and authors (some emails must be wrong though). The original [master](https://gitlab.com/GCstar/GCstar/tree/master) branch was pulled directly from upstream "trunk" (latest synchronisation **2017/05/15**). The [main](https://gitlab.com/GCstar/GCstar/tree/main) branch is now the Gtk3 version of GCstar as Gtk2 is deprecating. The [Gtk2](https://gitlab.com/GCstar/GCstar/tree/Gtk2) branch should be used for environments limited to Gtk2, most evolutions to GCstar are still backported in this branch. As the original SVN repository is now unavailable, this GIT repository seems to be the best way to get the source code of GCstar.

As the original documentation is temporily unavailable, a [new version](https://gcstar.gitlab.io/gcstar_docs) was initiated in the [GCstar_Docs](https://gitlab.com/GCstar/gcstar_docs) Gitlab repository but it's still in an early and incomplete stage. Support may be found in forums ( for example, https://forum.ubuntu-fr.org/viewtopic.php?id=1919911 ) or by creating [an issue](https://gitlab.com/GCstar/GCstar/-/issues/new) on Gitlab.
 
GCstar can be used with 3 Android apps:

* [GCstar Viewer](https://f-droid.org/fr/packages/com.gcstar.viewer/) to browse collections
* [GCstar Scanner](https://f-droid.org/fr/packages/com.gcstar.scanner/) and [GCstar Scan NG](https://gitlab.com/GCstar/GCstar_Scan_NG) (newer version) to add items to a collection by scanning their barcode

# GCstar
Gestionnaire de collection (films, musiques, livres, jeux, ...)

## Logiciel

GCstar est écrit en Perl avec des extension Gtk : il tourne sur Linux, Windows et MacOS.

## Projet

Le projet d'origine créé par Tian est [GCstar](http://www.gcstar.org/). Le code source en license GPL était disponible sur un [dépôt SVN](http://gna.org/projects/gcstar) (fermé en mai 2017). Jusque récemment, il existait un [forum de discussion](http://forums.gcstar.org/) dédié mais il est temporairement fermé pour des raisons de sécurité.

GCstar utilise un système de plugins pour collecter de l'information sur des sites web. Les développements de ces plugins sont actuellement très répartis et accessibles via des forums ( https://forum.ubuntu-fr.org/viewtopic.php?id=1919911 , http://forums.gcstar.org/ ) ou sur des forges.

Ce dépôt a été créé à partir de la source SVN en utilisant **git-svn**, en essayant de préserver les tags et les auteurs (sans doute des erreurs dans les adresses mail). La branche d'origine [master](https://gitlab.com/GCstar/GCstar/tree/master) était destinée à rester synchronisée avec le "trunk" de SVN (dernière synchro **2017/05/15**). La branche principale [main](https://gitlab.com/GCstar/GCstar/tree/main) contient maintenant la version Gtk3 étant donnée l'obsolescence en cours de Gtk2. La branche [Gtk2](https://gitlab.com/GCstar/GCstar/tree/Gtk2) est la version à utiliser pour les environnements ne disposant que de Gtk2, la plupart des évolutions de GCstar y sont toujours intégrées.  Le dépôt SVN n'étant plus accessible (serveur fermé), ce dépôt GIT est désormais la référence pour le code source de GCstar. 

GCstar peut être utilisé avec trois applications Android :

* [GCstar Viewer](https://f-droid.org/fr/packages/com.gcstar.viewer/) pour visualiser une collection
* [GCstar Scanner](https://f-droid.org/fr/packages/com.gcstar.scanner/) et [GCstar Scan NG](https://gitlab.com/GCstar/GCstar_Scan_NG) (version plus récente) pour entrer des éléments dans une collection à partir de leur code barre.

Comme la documentation d'origine est actuellement indisponible, une [nouvelle version](https://gcstar.gitlab.io/gcstar_docs) a été initialisée dans le dépôt Gitlab [GCstar_Docs](https://gitlab.com/GCstar/gcstar_docs) mais elle est encore très incomplète. De l'aide sur GCstar peut être obtenue via des forums ( par exemple, https://forum.ubuntu-fr.org/viewtopic.php?id=1919911 ) ou en crééant une [issue](https://gitlab.com/GCstar/GCstar/-/issues/new) sur Gitlab.

# Installation

The newest versions of GCstar are not packaged and require a manual [installation](resources/Installation.md).

La dernière version de GCstar nécessite une [installation](resources/Installation.md) manuelle.

# Plugins

GCstar uses a plugin architecture to get information on items of collections from web sites. These plugins can easily be modified to adapt to changes of the web sites and can be updated without reinstalling GCstar.

A [list of plugins and their status](resources/Plugins.md) is used to keep track of changes.

GCstar est basé sur une architecture à base de modules complémentaires (plugins) permettant d'enrichir les informations à partir de sites web. Ces modules peuvent facilement être modifiés pour prendre en compte les évolutions des sites et peuvent être mis à jour sans réinstaller GCstar.

Une [liste des plugins](resources/Plugins.md) est utilisée pour tracer leur disponibilité et leurs évolutions.

# Contributions

Software contributions are welcome on this Gitlab repository or in the [GCstar forum](http://forums.gcstar.org/). Some help is also need to [check or contribute translations](resources/Translations.md) for already supported languages or new languages.

Les contributions au code de GCstar sont les bienvenues sur ce dépôt Gitlab ou sur le [forum GCstar](http://forums.gcstar.org/). De l'aide est aussi nécessaire pour [vérifier ou compléter les traductions](resources/Translations.md) pour les langues déjà supportée ou pour de nouvelles traductions.





