package GCImport::GCImportDataCrow;

###################################################
#
#  Copyright 2018 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;
use GCImport::GCImportBase;
{
    package GCImport::GCImporterDataCrow;

    use base qw(GCImport::GCImportBaseClass);

    use File::Basename;
    use File::Copy;
    use XML::Simple;

    use GCUtils 'localName';

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);
        return $self;
    }

    sub getName
    {
        return "DataCrow (.xml)";
    }

    sub getFilePatterns
    {
       return (['DataCrow (.xml)', '*.xml']);
    }

    sub getModelName
    {
        my $self = shift;
        return $self->{model}->getName;
    }

    sub getOptions
    {
        return [];
    }

    sub getEndInfo
    {
        return "";
    }

    sub getPictureName
    {
        my ($self, $pictureName, $filename, $dir, $title) = @_;

        $pictureName =~ s/.*$filename/$dir$filename/;
        $pictureName =~ s/file:\/\/\///;
        $pictureName =~ s/\\/\// if ($^O =~ /win32/i);
        if (-e localName($pictureName))
        {
            my $pic = $self->{options}->{parent}->getUniqueImageFileName('.jpg', $title);
            copy localName($pictureName), localName($pic);
            return $pic;
        }
        return $pictureName;
    }

    sub getItemsArray
    {
        my ($self, $file) = @_;

        my $parent = $self->{options}->{parent};
        $self->{modelsFactory} = $parent->{modelsFactory};
        $self->{modelAlreadySet} = 0;
        my $model = $self->getModelName();

        my $xml = XML::Simple->new;
        my $data = $xml->XMLin (
            localName($file),
            forceArray => ['book','authors', 'publishers', 'movie'],
            suppressEmpty => "",
            KeyAttr    => ['']
        );

        my @result;
        
        my($filename, $dirs, $suffix) = fileparse($file);
        $filename =~ s/.xml//;

        if ($model eq 'GCbooks')
        {
            my $book;

            foreach $book(@{$data->{book}}){
                my $item;

                $item->{title} = $book->{title};
                $item->{date} = $book->{created};
                $item->{publication} = $book->{year};
                $item->{description} = $book->{description};
                $item->{webpage} = $book->{webpage};
                $item->{pages} = $book->{pages};
                $item->{isbn} = $book->{'isbn-13'};
                $item->{genre} = $book->{category};
                $item->{authors} = $book->{'authors-list'};
                $item->{tags}      = $book->{'tags-list'};
                $item->{location}  = $book->{'container-list'};
                $item->{publisher} = $book->{'publishers-list'};
                $item->{web}  = $book->{webpage};
                $item->{cover} = $book->{'picture-front'};
                # get cover pictures
                my $pictureName = $book->{'picture-front'};
                $pictureName =~ s/.*$filename/$dirs$filename/;
                $pictureName =~ s/\\/\// if ($^O =~ /win32/i);
                $item->{cover} = $self->getPictureName($book->{'picture-front'}, $filename, $dirs, $book->{title});

                push @result, $item;
            }
        }
        elsif ($model eq 'GCfilms')
        {
            my $movie;

            foreach $movie(@{$data->{movie}})
            {
                my $item;

                $item->{title} = $movie->{title};
                $item->{original} = $movie->{'title-local'};
                $item->{synopsis} = $movie->{description};
                $item->{director} = $movie->{'directors-list'};
                $item->{genre}    = $movie->{'genres-list'};
                $item->{actors}   = $movie->{'actors-list'};
                $item->{tags}     = $movie->{'tags-list'};
                $item->{place}    = $movie->{'container-list'};
                $item->{webPage}  = $movie->{webpage};
                $item->{added} = GCUtils::strToTime($movie->{created},"%Y-%m-%e",undef);
                if ($movie->{playlength} =~ m/(\d+):(\d+):(\d+)/)
                {
                   $item->{time} = 60 * $1 + $2;
                }
                $item->{rating}   = $movie->{rating};
                $item->{date}     = $movie->{year};
                $item->{image} = $self->getPictureName($movie->{'picture-front'}, $filename, $dirs, $movie->{title});

                push @result, $item;
            }
        }
        return \@result;
    }
}

1;
