# GCstar installation

GCstar is based on the Perl language and the GTK3 library.

## Warning

Before installing a new version of GCstar, don't forget to make some backups of all your collections!

## Linux

On some Linux distributions, GCstar is available as a package and can be installed using the application manager or with a command line. GCstar can be installed from the source code using the following instructions.

### Debian based distributions

#### Dependencies installation with *apt* (Debian based distributions)

Execute the following commands (or similar commands according to the Linux distribution) to install Perl and the mandatory libraries for a minimalistic GCstar (no plugins)

````
sudo apt-get install perl
sudo apt-get install libgtk3-perl
sudo apt-get install libgtk3-simplelist-perl
sudo apt-get install libxml-simple-perl
````

To use plugins (to download information from the web), install additional libraries

````
sudo apt-get install libjson-perl
sudo apt-get install libdatetime-format-strptime-perl
sudo apt-get install liblocale-codes-perl
````

#### Download and prepare the installation

In the target directory for GCstar, execute the following commands 

````
wget -O - https://gitlab.com/GCstar/GCstar/-/archive/main/GCstar-main.tar.gz | tar xzf -
mv GCstar-main GCstar
````

You may test GCstar by

````
cd GCstar/gcstar/bin
perl gcstar
cd ..
````

#### Installation using a deb package ####

In the *GCstar/gcstar* directory

````
cp -r packages/debian .
dpkg-buildpackage -rfakeroot -b -uc -us --no-sign
cd ..
ls gcstar_*.deb | sort -V | tail -n1 | xargs sudo apt-get --fix-broken install
````

To run GCstar, simply type

````
gcstar
````

If GCstar was already installed, copy the **gcstar** script in the appropriate binaries directory (probably **/usr/bin**, check with the "which gcstar" command) and copy the **lib** directory in the libraries directory (probably **/usr/lib/gcstar** or **/usr/share/gcstar/lib**).

## Dependencies

To run a minimal GCstar, the following *Perl* modules are required

````
Gtk3
Gtk3::SimpleList
XML::Simple
````

To get a fully functional version of GCstar (statistics, importation/exportation, archives), the following Perl modules are required

````
Archive::Tar
Archive::Zip
Compress::Zlib
Date::Calc
DateTime::Format::Strptime
Digest::MD5
GD
GD::Graph
GD::Text
Image::ExifTool
MIME::Base64
MP3::Info
MP3::Tag
Net::FreeDB
Ogg::Vorbis::Header::PurePerl
Time::Piece
````

On some operating systems, some Perl librairies may not be packaged and may require a manual CPAN installation, for example:

````
cpan install XML::Simple
cpan install DateTime::Format::Strptime
cpan install JSON
````

## Windows 

Windows installers for GCstar are available for [1.7.2](https://gitlab.com/GCstar/GCstar/-/tags/v1.7.2) and [1.7.3](https://gitlab.com/GCstar/GCstar/-/tags/v1.7.3) versions. GCstar used to be installed from the source code using the following instructions but some external libraries are no longer available.


### Manual installation using *ActivePerl* 

*Warning: the repository www.sisyphusion.tk is currently unavailable!*

- Install Perl from the ActivePerl website
- Add a package repository: ````ppm repo add http://www.sisyphusion.tk/ppm/````
- Install the GTK3 library :  ````ppm install gtk3````
- Extract an archive of the current dev version ````https://gitlab.com/Kerenoc/GCstar/repository/Test/archive.zip````
- From the newly created directory go into the ````gcstar\bin```` sub-directory
- Execute ````perl gcstar````

### Manual installation using *Strawberry Perl*

- Follow the first part of the instructions available on GitHub ````https://github.com/tothi/gcstar-win32````
 
### Updating after installation ###

- Install GCstar using the installer
- Download the [latest source code](https://gitlab.com/GCstar/GCstar/-/archive/main/GCstar-main.zip for the Gtk3 version) from Gitlab
- Unzip the archive in a temporary folder
- Copy the sub-folder **gcstar/lib/gcstar** under this folder to the sub-folder **lib** under the folder where you installed GCstar (for example "c:\Program Files\GCstar\lib")
- Run GCstar to benefit from the updates and latest plugins


