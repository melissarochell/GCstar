<project name="GCstar" default="generate" basedir=".">
    <description>
        GCstar build file
    </description>

    <property name="version" value="1.7.3"/>
    <property name="gcstar_description" value="GCstar - An application for managing your personal collections"/>

    <property name="build.root" value=".." />
    <property name="build.dist" location="${build.root}/dist"/>
    <property name="build.bin"  location="${build.root}/bin"/>
    <property name="build.rpm"  location="${build.root}/RPM"/>

    <property file="${user.home}/.config/gcstar.build"/>

    <property name="src" location="${build.root}/gcstar"/>
    <property name="mailer" value="${build.root}/tools/generateMail.pl"/>
    <property name="versionfile" location="${src}/bin/gcstar"/>
    <property name="header.inc"  location="${build.site}/header.inc.php"/>
    <property name="rpmtmp"  location="${build.root}/rpm_tmp"/>
    <property name="rpminst" value="/usr"/>
    <property name="rpmsources" location="${build.rpm}/SOURCES"/>
    <property name="rpmurl" value="http://download.gna.org/gcstar/Rpm/"/>
    <property name="win32" location="${build.dist}/win32"/>
    <property name="fc.releasever" value="31"/>
    <property name="fedora" location="${build.dist}/fedora/${fc.releasever}"/>
    <property name="debian" location="${build.dist}/debian"/>
    <property name="extras" location="${build.dist}/extras/gcstar/FC-${fc.releasever}"/>
    <property name="createrepo" value="/usr/bin/createrepo"/>
    <property name="makeself" value="${build.bin}/makeself.sh"/>
    <property name="tmpdir" value="/tmp/gcstar_build"/>
    <property name="makensis" value="/usr/bin/makensis"/>
    <property name="nsis" value="${win32}/gcstar.nsi"/>
    <property name="extrasScript" value="${user.home}/bin/generateNewGCstarFedoraExtrasVersion"/>

    <target name="clean">
            <delete dir="${build.dist}" />
            <delete dir="${build.rpm}" />
            <delete>
                    <fileset dir="${src}/bin">
                            <include name="*.out"/>
                            <include name="*.prof"/>
                    </fileset>
            </delete>
    </target>

    <target name="version">
            <replaceregexp file="${src}/packages/rpm/gcstar.spec" match="Version:.*" replace="Version: ${version}"/>
            <replaceregexp file="${src}/packages/fedora/gcstar.spec" match="Version:.*" replace="Version:        ${version}"/>
            <replaceregexp file="${src}/packages/win32/gcstar.nsi" match="PRODUCT_VERSION (.)[-a-z0-9.]+(.)" replace="PRODUCT_VERSION \1${version}\2"/>
            <replaceregexp file="${versionfile}" match="my \$VERSION = '.*';" replace="my $VERSION = '${version}';"/>
            <!--<replaceregexp file="${header.inc}" match="(\$version = ')[0-9]+\.[0-9a-z]+" replace="\1${version}'" flags="gi" encoding="ISO-8859-1"/>-->
    </target>

      <target name="targz" depends="version">
              <tar tarfile="${build.dist}/gcstar-${version}.tar.gz" compression="gzip">
                      <tarfileset dir="${src}" includes="**" excludes="install" prefix="gcstar"/>
                      <tarfileset dir="${src}" includes="install" prefix="gcstar" mode="755"/>
            </tar>
              <copy file="${build.dist}/gcstar-${version}.tar.gz" tofile="${build.dist}/gcstar.tar.gz"/>
      </target>

    <target name="makeself" depends="targz">
            <untar
                    src="${build.dist}/gcstar-${version}.tar.gz"
                    dest="${tmpdir}"
                    compression="gzip"/>
            <chmod file="${tmpdir}/gcstar/install" perm="+x"/>
            <exec executable="${makeself}">
                <arg value="${tmpdir}/gcstar"/>
                <arg value="${build.dist}/gcstar-${version}.run"/>
                <arg value="${gcstar_description}"/>
                <arg value="./install"/>
            </exec>
    </target>

    <target name="rpm" depends="version">
            <mkdir dir="${build.rpm}" />
            <copy todir="${build.rpm}/SPECS" file="${src}/packages/rpm/gcstar.spec"/>
            <delete dir="/var/tmp/gcstar-root/" />
            <delete dir="${rpmtmp}${rpminst}"/>
            <mkdir dir="${rpmtmp}${rpminst}"/>
            <copy todir="${rpmsources}" file="${build.dist}/gcstar-${version}.tar.gz"/>
            <rpm specfile="gcstar.spec" topdir="${build.rpm}" cleanbuilddir="false"/>
            <copy todir="${build.dist}" file="${build.rpm}/RPMS/noarch/gcstar-${version}-1.noarch.rpm"/>
            <copy tofile="${build.dist}/gcstar-dev-1.noarch.rpm" file="${build.rpm}/RPMS/noarch/gcstar-${version}-1.noarch.rpm"/>
            <delete dir="${rpmtmp}"/>
            <delete file="${build.rpm}/SPECS/gcstar.spec"/>
    </target>

    <target name="fedora" depends="version, targz">
            <copy todir="${build.rpm}/SPECS" file="${src}/packages/fedora/gcstar.spec"/>
		<copy todir="${build.rpm}/SOURCES" file="${src}/packages/fedora/patch-install"/>
		<copy todir="${build.rpm}/SOURCES" file="${src}/packages/fedora/patch-gcstar"/>
            <delete dir="/var/tmp/gcstar-${version}-*-root-${user.name}/" />
            <copy file="${build.dist}/gcstar-${version}.tar.gz" todir="${rpmsources}"/>
            <rpm specfile="gcstar.spec" topdir="${build.rpm}" cleanbuilddir="true" command="-ba"/>
            <copy todir="${fedora}">
                    <fileset dir="${build.rpm}/RPMS/noarch/">
                            <include name="gcstar-${version}-*.noarch.rpm"/>
                    </fileset>
            </copy>
            <delete file="${build.rpm}/SPECS/gcstar.spec"/>
    </target>

    <target name="extras" depends="fedora">
            <exec executable="${extrasScript}">
                    <arg value="${fc.releasever}"/>
                    <arg value="${build.rpm}/SRPMS/gcstar-${version}-1.fc${fc.releasever}.src.rpm"/>
            </exec>
    </target>

    <target name="yum" depends="clean, fedora">
            <copy todir="${fedora}">
                    <fileset dir="${build.rsync}/fedora/${fc.releasever}/">
                            <include name="gcstar-*"/>
                    </fileset>
            </copy>
            <exec executable="${createrepo}">
                    <arg value="-p"/>
                <arg value="${fedora}"/>
            </exec>
    </target>

    <target name="prepare_rsync" depends="rpm, targz">
            <copy todir="${build.rsync}" file="${build.dist}/gcstar-${version}.tar.gz"/>
            <copy todir="${build.rsync}/installers" file="${build.dist}/gcstar-${version}.run"/>
            <copy todir="${build.rsync}/Rpm" file="${build.dist}/gcstar-${version}-1.noarch.rpm"/>
    </target>

    <target name="upload" depends="prepare_rsync">
            <exec executable="rsync">
                    <arg value="-avr"/>
                    <arg value="--rsh=ssh"/>
                    <arg value="${build.rsync}/"/>
                    <arg value="${ssh.user}@download.gna.org:/upload/gcstar"/>
            </exec>
    </target>

    <target name="mail">
            <exec executable="${mailer}">
                <arg value="${version}"/>
            </exec>
    </target>

    <target name="debian" depends="version">
            <delete dir="${debian}/gcstar"/>
            <mkdir dir="${debian}/gcstar"/>
            <copy todir="${debian}/gcstar">
                <fileset dir="${src}">
                        <include name="*"/>
                        <include name="lib/**"/>
                        <include name="bin/*"/>
                        <include name="share/**"/>
                </fileset>
            </copy>
            <mkdir dir="${debian}/gcstar/debian"/>
            <copy todir="${debian}/gcstar/debian">
                    <fileset dir="${src}/packages/debian">
                            <include name="**"/>
                    </fileset>
            </copy>
            <chmod file="${debian}/gcstar/install" perm="+x"/>
	    <exec executable="dpkg-buildpackage" dir="${debian}/gcstar">
                <arg value="-rfakeroot"/>
                <arg value="-b"/>
                <arg value="-uc"/>
                <arg value="-us"/>
                <arg value="--no-sign"/>
            </exec>
            <copy todir="${build.dist}">
                <fileset dir="${debian}">
                  <include name="*.deb"/>
                </fileset>
            </copy>
    </target>

    <target name="generate" depends="version, clean, targz, makeself, rpm" />
    <target name="all" depends="generate, fedora, debian" />
    <target name="deliver" depends="version, generate, upload, mail"/> <!--, extras, mail" />-->

</project>
